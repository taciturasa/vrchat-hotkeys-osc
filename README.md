[![pipeline status](https://gitlab.com/ameliend/vrchat-hotkeys-osc/badges/main/pipeline.svg)](https://gitlab.com/ameliend/vrchat-hotkeys-osc/-/commits/main)
[![pylint](https://gitlab.com/ameliend/vrchat-hotkeys-osc/-/jobs/artifacts/main/raw/public/badges/pylint.svg?job=pylint)](https://gitlab.com/ameliend/vrchat-hotkeys-osc/-/commits/main)
[![Made with: pycharm](https://img.shields.io/badge/made%20with-Pycharm-1fcc82)](https://www.jetbrains.com/fr-fr/pycharm/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

# VRChat Hotkeys OSC

<p align="center">
  <img src="./vrchat_hotkeys_osc/resources/banner.png">
</p>

A VRChat OSC software that allow you to **create Hotkeys and send OSC signals to VRChat** 
using your **keyboard** to control your avatar parameters.

# ✅ [Demonstration video Click here!](https://youtu.be/Ur3breZN2Pw)

# ✅ [Setup demonstration video Click here!](https://youtu.be/2cf9lABm0m4)

### 🔎 [More information about OSC for Avatars](https://hello.vrchat.com/blog/vrchat-osc-for-avatars)

## 🚀 [Download the latest release here!](https://drive.proton.me/urls/5TBKFPPTE4#EpccP0tUsePU)

### 📦 [Older releases here!](https://drive.proton.me/urls/80TVF3PB64#IsUlAxlIQzqx)

## 👀 Nice, but what is the purpose?

It was first created for me, a desktop player :)

I use a **SoundPad** very often with predefined text to speech, I have more than 300+ different sentences, 
on which I have assigned hotkeys to trigger some of them.

So... I have a lot of keyboard combinations to interact with other players,
it's very fun and often very interesting :)

But with the new OSC feature, I saw an opportunity to greatly improve my roleplay, 
by **controlling my avatar animations with keyboard combinations** like **SoundPad**.

Now combined, I can trigger animations, toggle objects... **in the same time as I use my SoundPad**.

## 🌟 Features

<p align="center">
  <img src="./vrchat_hotkeys_osc/resources/screenshot.png">
</p>

* Automatic avatars parameters detection (depending on which avatar you choose in game, 
hit the refresh button)
* Input delay
* Set single value (Boolean, Integer, or Float)
* Toggle between two value (Boolean, Integer, or Float)
* Switch back delay for toggle 
* Open / Save your configuration.

## 🛠 How does it work?

Make sure your OSC feature is enabled in VRChat.

<p align="center">
  <img src="./vrchat_hotkeys_osc/resources/activate.jpg">
</p>

At each avatar change, VRChat will generate a ".json" file, located in the appdata folder of VRChat,
containing the available parameters of the avatar.
Hotkeys OSC will simply read these files.

After creating some hotkeys, just activate the listener.
When a hotkey is pressed, then an OSC signal will be sent to VRChat depending on the hotkey parameters.

## 🧨 Feature request of find a bug?

You can contact me on Discord Nekosan#6408, or better, create a ticket in this repository!
